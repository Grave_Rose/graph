#!/bin/bash
#
# Gr@ve_Rose's Assisted PHishing
################################
#
# Variables start here
######################
#
LOGFILE="graph-$(date +%d.%m.%Y-%H:%M.%S).log"
VERSION="0.3"
#
# Program required below. Do not change!
########################################
#
DEPS=(ssmtp sed awk sort wc md5sum grep)

# Log that we've started
########################
echo "[$(date +%d.%m.%Y-%H:%M.%S)] - GRAPH v.$VERSION started" > $LOGFILE

# Start with a clear screen
clear
echo

echo -e "
\e[34m               @@@@@@@@  @@@@@@@    @@@@@@   @@@@@@@   @@@  @@@  
              @@@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@  @@@  @@@  
              !@@        @@!  @@@  @@!  @@@  @@!  @@@  @@!  @@@  
              !@!        !@!  @!@  !@!  @!@  !@!  @!@  !@!  @!@  
              !@! @!@!@  @!@!!@!   @!@!@!@!  @!@@!@!   @!@!@!@!  
\e[36m              !!! !!@!!  !!@!@!    !!!@!!!!  !!@!!!    !!!@!!!!  
              :!!   !!:  !!: :!!   !!:  !!!  !!:       !!:  !!!  
              :!:   !::  :!:  !:!  :!:  !:!  :!:       :!:  !:!  
               ::: ::::  ::   :::  ::   :::   ::       ::   :::  
               :: :: :    :   : :   :   : :   :         :   : :

\e[39m               [ Gr@ve_Rose's Assisted PHishing ]
               v.$VERSION
               "
echo 

# Check for programns and exit if not found
###########################################
echo -e "\e[36m[ Dependency Check Phase ]\e[39m"
echo
# Check for all programs in "DEPS" array
########################################
for i in "${DEPS[@]}"; do
  echo -ne "\e[36mChecking for $i ... "
  PROG=$(which $i)
  RV=$?
  if [ "$RV" != "0" ]; then
    echo -e "\e[91mNot found!"
    echo -e "\e[39mPlease install $i."
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - $i not installed! Exiting..." >> $LOGFILE
    echo
    exit 255
  else
    echo -e "\e[92mFound!"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - $i found." >> $LOGFILE
  fi
done

# Check to see if all options were passed
# by the user and exit if they weren't.
#########################################
while getopts ":i:f:xh" opt; do
  case ${opt} in
    i)
      ARG_I=$OPTARG
      ;;
    f)
      ARG_F=$OPTARG
      ;;
    x)
      ARG_X="1"
      ;;
    h)
      echo "Usage: graph.sh -i <input file> -f <field separator> [-x]"
      echo
      exit 100
      ;;
    \?)
      echo "Usage: graph.sh -i <input file> -f <field separator> [-x]"
      echo
      exit 101
      ;;
  esac
done

function checks () {
  if [ -z $ARG_I ]; then
    echo -e "\e[91mError: \e[92mMissing -i"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Missing Input File! Exiting..." >> $LOGFILE
    exit 253
  fi
  if [ -z $ARG_F ]; then
    echo -e "\e[91mError: \e[92mMissing -f"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Missing Field Separator! Exiting..." >> $LOGFILE
    exit 253
  fi
  echo

  # File validation and backups
  #############################
  echo -e "\e[36m[ File Check Phase ]\e[39m"

  # Make sure ARG_I exists
  ########################
  if [ ! -f $ARG_I ]; then
    echo -e "\e[91mError:\e[39m $ARG_I does not exist! Exiting..."
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - $ARG_I does not exist! Exiting..." >> $LOGFILE
    echo
    exit 1
  fi

  # Check for /etc/ssmtp/ssmtp.conf
  #################################
  SSMTP=/etc/ssmtp/ssmtp.conf
  if [ ! -f $SSMTP ]; then
    echo -e "\e[91mError:\e[39m $SSMTP does not exist. Exiting..."
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - $SSMTP missing! Exiting..." >> $LOGFILE
    echo
    exit 1
  fi

  # Make a backup and new one
  ###########################
  echo -n -e "\e[36mBacking up $SSMTP to $SSMTP.orig ... "
  cp $SSMTP $SSMTP.orig
  RV=$?
  if [ "$RV" != "0" ]; then
    echo -e "\e[91mFailed!\e[39m"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Failed to copy $SSMTP to $SSMTP.orig" >> $LOGFILE
    exit
  fi
  echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Copied $SSMTP to $SSMTP.orig" >> $LOGFILE
  sed -i -e '/^$/d' $SSMTP
  sed -i -e '/^#/d' $SSMTP
  echo -e "\e[92mDone!\e[39m"

  # Backup original CSV
  #####################
  echo -n -e "\e[36mBacking up $ARG_I to $ARG_I.orig ... "
  cp $ARG_I $ARG_I.orig
  RV=$?
  if [ "$RV" != "0" ]; then
    echo -e "\e[91mFailed!\e[39m"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Failed to copy $ARG_I to $ARG_I.orig" >> $LOGFILE
    exit
  else
    echo -e "\e[92mDone!\e[39m"
  fi

  # Count the number of fileds in the
  # CSV to make sure we have three.
  #
  # Only applies if "-x" is NOT set
  ##################################
  if [ "$ARG_X" != "1" ]; then
    echo -n -e "\e[36mChecking all lines in $ARG_I to make sure each has three fields ... \e[39m"
    while read line; do
      NUM_FIELDS=$(echo $line | awk -F "$ARG_F" '{print NF}')
      if [ $NUM_FIELDS != 3 ]; then
        echo -e "\e[91mError:\e[39m Line \"$line\" does not have three fields. Exiting..."
        echo "[$(date +%d.%m.%Y-%H:%M.%S)] - $line does not have three fields! Exiting..." >> $LOGFILE
        echo 
        exit 200
      fi
    done < $ARG_I
    echo -e "\e[92mValid!\e[39m"
  fi

  # Create a list of groups
  ##############################
  awk -F "$ARG_F" '{print $3}' $ARG_I | sort -u > groups.list
}

function inputs () {
  # Have the user fill in information for each
  # department phishing e-mail.
  ############################################
  echo
  echo -e "\e[36m[ Group Configuration Phase ]\e[39m"
  # Read the input file and get the number
  # of groups
  ########################################
  DEPT_NUM=$(awk -F "$ARG_F" '{print $3}' $ARG_I | sort -u | wc -l)
  echo "Number of groups: $DEPT_NUM"
  echo "[$(date +%d.%m.%Y-%H:%M.%S)] - About to configure $DEPT_NUM group(s)" >> $LOGFILE
  echo
  i=1
  while IFS=$ARG_F read -u 3 line; do
    NUM_USER_DEP=$(grep "$line" $ARG_I | wc -l)
    echo -e "\e[36m$line (Group $i of $DEPT_NUM)"
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Configuring group $line" >> $LOGFILE
    echo -e "($NUM_USER_DEP Users)\e[39m"

    SSMTP_FROM=$(grep -i root\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "What is the e-mail address the e-mail is \"from\" [$SSMTP_FROM]: "
    read DEP_EMAIL_FROM
    if [ "$DEP_EMAIL_FROM" == "" ]; then
      DEP_EMAIL_FROM=$SSMTP_FROM
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_EMAIL_FROM to $DEP_EMAIL_FROM" >> $LOGFILE

    SSMTP_SRV=$(grep -i mailhub\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "What is the \"server:port\" of your SMTP server [$SSMTP_SRV]: "
    read DEP_SSMTP_SRV
    if [ "$DEP_SSMTP_SRV" == "" ]; then
      DEP_SSMTP_SRV=$SSMTP_SRV
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_SRV to $DEP_SSMTP_SRV" >> $LOGFILE

    SSMTP_TLS=$(grep -i UseTLS\= $SSMTP | awk -F '=' '{print $2'})
    echo -n "Does the server  ($DEP_SSMTP_SRV) use TLS [$SSMTP_TLS]: "
    read DEP_SSMTP_TLS
    if [ "$DEP_SSMTP_TLS" == "" ]; then
      DEP_SSMTP_TLS=$SSMTP_TLS
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_TLS to $DEP_SSMTP_TLS" >> $LOGFILE

    SSMTP_STARTTLS=$(grep -i UseSTARTTLS\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "Does the server ($DEP_SSMTP_SRV) use STARTTLS [$SSMTP_STARTTLS]: "
    read DEP_SSMTP_STARTTLS
    if [ "$DEP_SSMTP_STARTTLS" == "" ]; then
      DEP_SSMTP_STARTTLS=$SSMTP_STARTTLS
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_STARTTLS to $DEP_SSMTP_STARTTLS" >> $LOGFILE

    SSMTP_REWRITE=$(grep -i rewritedomain\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "What domain should we re-write for [$SSMTP_REWRITE]: "
    read DEP_SSMTP_REWRITE
    if [ "$DEP_SSMTP_REWRITE" == "" ]; then
      DEP_SSMTP_REWRITE=$SSMTP_REWRITE
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_REWRITE to $DEP_SSMTP_REWRITE" >> $LOGFILE

    SSMTP_USER=$(grep -i authuser\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "What username do we use to authenticate to the server [$SSMTP_USER]: "
    read DEP_SSMTP_USER
    if [ "$DEP_SSMTP_USER" == "" ]; then
      DEP_SSMTP_USER=$SSMTP_USER
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_USER to $DEP_SSMTP_USER" >> $LOGFILE

    SSMTP_PASS=$(grep -i authpass\= $SSMTP | awk -F '=' '{print $2}')
    echo -n "What is the password for the user $DEP_SSMTP_USER [$SSMTP_PASS]: "
    read DEP_SSMTP_PASS
    if [ "$DEP_SSMTP_PASS" == "" ]; then
      DEP_SSMTP_PASS=$SSMTP_PASS
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_SSMTP_PASS to $DEP_SSMTP_PASS" >> $LOGFILE

    echo -n "What is this common name of the person the e-mail is \"from\" [John Doe]: "
    read DEP_FROM
    if [ "$DEP_FROM" == "" ]; then
      DEP_FROM="John Doe"
    fi
    PERSON=$DEP_FROM
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_FROM to $DEP_FROM" >> $LOGFILE

    echo -n "Who should we CC in this e-mail (ex: John Doe <jdoe@example.com>) [Leave blank for none]: "
    read DEP_CC
    if [ "$DEP_CC" == "" ]; then
      DEP_CC=""
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_CC to $DEP_CC" >> $LOGFILE

    echo -n "What is the name of your e-mail template [test.html]: "
    read DEP_HTML
    if [ "$DEP_HTML" == "" ]; then
      DEP_HTML="test.html"
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_HTML to $DEP_HTML" >> $LOGFILE

    echo -n "What is the target URL [https://www.google.com/site.html?]: "
    read DEP_URL
    DEP_URL=$(echo $DEP_URL | sed 's~/~+~g')
    if [ "$DEP_URL" == "" ]; then
      DEP_URL="https:++www.google.com"
    fi
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Setting \$DEP_URL to $DEP_URL" >> $LOGFILE

    # echo -n "Adding MD5 Hash ... "
    ## FIXME
    #
    # The HASH below is pulling all e-mail addresses
    # and MD5'ing them together.
    # Create a while loop to go through all instances
    # and hash the e-mails that way.
    # HASH=$(grep "$line" $ARG_I | awk -F "$ARG_F" '{print $2}' | md5sum)
    HASH="HASH"
    # echo "Done!"
    echo -n "Updating lists..."
    sed -i -e "/$line/s/$/$ARG_F$DEP_FROM <$DEP_EMAIL_FROM>$ARG_F$DEP_CC$ARG_F$DEP_HTML$ARG_F$DEP_URL$ARG_F$HASH$ARG_F$PERSON/" $ARG_I
    sed -i -e 's///g' $ARG_I
    echo "Done!"
    echo
    echo -n "Creating $SSMTP.$line ... "
    echo "root=$DEP_EMAIL_FROM" > $SSMTP.$line
    echo "mailhub=$DEP_SSMTP_SRV" >> $SSMTP.$line
    echo "rewritedomain=$DEP_SSMTP_REWRITE" >> $SSMTP.$line
    echo "usetls=$DEP_SSMTP_TLS" >> $SSMTP.$line
    echo "usestarttls=$DEP_SSMTP_STARTTLS" >> $SSMTP.$line
    echo "fromlineoverride=YES" >> $SSMTP.$line
    echo "authuser=$DEP_SSMTP_USER" >> $SSMTP.$line
    echo "authpass=$DEP_SSMTP_PASS" >> $SSMTP.$line
    echo "Done"
    echo

    i=$[$i+1]

  done 3< groups.list

  # MD5 E-mail addresses and replace the
  # word HASH with the actual hash.
  ######################################
  if [ -f temp.list ]; then
    rm -f temp.list
  fi
  touch temp.list
  while IFS=$ARG_F read -u 4 line; do
    # FIRST=$(echo $line | awk -F "$ARG_F" '{print $1}')
    HASH=$(echo $line | awk -F "$ARG_F" '{print $2}' | md5sum | sed 's/ -//g' | sed 's/ //g')
    NEWLINE=$(echo $line | sed "s/HASH/$HASH/g")
    echo $NEWLINE >> temp.list
  done 4< $ARG_I

  # Copy temp.list to ARG_I and remove
  ####################################
  mv temp.list $ARG_I
  rm -f temp.list

  # Clean-up the MD5 output.
  ##########################
  sed -i -e 's/ -//g' $ARG_I
  echo 

  # Change the + to /
  #####################
  sed -i -e 's~+~/~g' $ARG_I
  echo -n "Do you want to change these values? [y/N] "
}

function launch () {
  # Ground control to Major Tom
  #############################
  echo -e "\e[36m[ Launch Preparation Phase ]\e[39m"
  echo 
  echo -e "\e[91mCaution:\e[39m By proceeding, you will be launching a spam/phishing campaign."
  echo "You can press ^C (Control+C) to stop here and leave $ARG_I with the latest values"
  echo "which can then be launched later with \"-x\" set."
  echo 
  echo -n "Do you want to proceed? [y/N] "
  read PROCEED
  if [ "$PROCEED" == "y" ] || [ "$PROCEED" == "Y" ]; then
    echo
  else
    echo "Launch aborted. Exiting..."
    echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Launch aborted by user." >> $LOGFILE
    echo
    exit 1
  fi
  echo -n "Ground Control: T minus 5..."
  sleep 1
  echo -n "4..."
  sleep 1
  echo -n "3..."
  sleep 1
  echo -n "2..."
  sleep 1
  echo -n "1..."
  sleep 1
  echo "Launch"

  # Go through each line in $ARG_I and
  # create the e-mail template to send
  # then send it.
  #####################################
  while IFS=$ARG_F read NAME EMAIL GROUP FROM CC TEMPLATE URL HASH PERSON; do
    # echo "$NAME $EMAIL $GROUP $FROM $TEMPLATE $URL $HASH"
    cp $TEMPLATE $TEMPLATE.new
    sed -i -e "s~NAME~$NAME~g" $TEMPLATE.new
    sed -i -e "s~EMAIL~$EMAIL~g" $TEMPLATE.new
    sed -i -e "s~FROM~$FROM~g" $TEMPLATE.new
    if [ "$DEP_CC" == "" ]; then
      sed -i -e "/^Cc/d" $TEMPLATE.new
    else
      sed -i -e "s~CCOPY~$CC~g" $TEMPLATE.new
    fi
    sed -i -e "s~URL~$URL~g" $TEMPLATE.new
    sed -i -e "s~HASH~$HASH~g" $TEMPLATE.new
    sed -i -e "s~PERSON~$PERSON~g" $TEMPLATE.new

    # The following command just shows you what
    # was done to the template file. If you 
    # want to see it, uncomment it.
    #############################################
    # cat $TEMPLATE.new

    echo -n "Launching to $EMAIL ... "
    ssmtp -C $SSMTP.$GROUP -t < $TEMPLATE.new
    RV=$?
    if [ "$RV" != "0" ]; then
      echo "There was an error with this mission."
      echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Exit code $RV when trying to launch to $EMAIL." >> $LOGFILE
    else
      echo "Success!"
      echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Launch to $EMAIL successful." >> $LOGFILE
    fi
    rm $TEMPLATE.new
  done < $ARG_I
}

function cleanup () {
  echo "Cleaning-up launch bay..."
  echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Cleaning up..." >> $LOGFILE
  rm -f groups.list
  echo "Mission complete."
  echo "[$(date +%d.%m.%Y-%H:%M.%S)] - Mission complete." >> $LOGFILE
  echo
}

checks

if [ "$ARG_X" == "1" ]; then
  echo "Loading $ARG_I for launch."
  echo "Loading $ARG_I for launch." >> $LOGFILE
  echo -n "Verifying $ARG_I fields ... "
  NUM_FIELD=$(awk -F "$ARG_F" '{print NF}' $ARG_I)
  if [ "$NUM_FIELD" != "9" ]; then
    echo "Error: $ARG_I does not have 9 fields!"
    exit 255
  else
    echo "Done"
  fi
else
  inputs
  while read options; do
    case "$options" in
     y)
       cp $ARG_I.orig $ARG_I
       inputs;;
      *)
        echo
        break;;
    esac
  done
fi
launch
cleanup

# EOF
