# GRAPH

GRAPH is designed to take a CSV file of users names, e-mail addresses and groups to then create a phishing campaign which will launch at the end of the program.

  - Linear configuration
  - Will prompt before launching
  - Support for multiple groups
  - Each group will have it's own SSMTP configuration
  
# How Does it Work? What Does it Do?
GRAPH is aimed at RedTeams who need to send phishing campaigns against customers to test their security awareness against legitimate phishing attacks. By using GRAPH, you can easily launch the phishing campaign against the targets while having customization based on groups of users each getting their own phishing e-mail.

For example, let's say our client has two groups of people they'd like to target: Sales and Accounting. The client has given us a list of names and e-mail addresses broken up by group which we then add into a CSV file (name,e-mail,group). Create your individual HTML e-mails customized to each group based on the included template so that you're ready to go.

Make sure you have your SMTP server already configured and run ```graph.sh -i list.csv -f , ``` which will start the process. GRAPH will find the number of groups and ask you to configure the phishing campaign settings for each group. For each group, you can specify individual e-mail servers, the "person" sending them, the e-mail template to use as well as the custom URL you'd like to send them to. Once done, the CSV will be saved and you'll be prompted if you want to launch or not. If you choose not to, you can re-load your CSV to launch status with ```graph.sh -i list.csv -f , -x ```. Lastly, a five second countdown will appear just in case you want to stop it after which the phishing campaign will launch.

During the configuration phase, GRAPH will pull defaults from your ```/etc/ssmtp/ssmtp.conf``` file. For each group, a new ```/etc/ssmtp/ssmtp.conf.<group>``` file will be created so your original isn't lost.

# Requirements

  - An SMTP server you can send e-mails from
  - ssmtp
  - sed
  - awk
  - sort
  - wc
  - md5sum
  - grep
  - which

# Example Files
### CSV Example (Example_Users.csv)
User input should be a CSV (with whatever separator you want) with three columns:
```
Full Name,user@example.com,%Group1%
Full Name2,user2@example.com,%Group_Two%
Full Name3,user3@example.com,%Group_Two%
```
Make sure that the group has the percentage signs on either side.

The first column **must** be the target's name, the second **must** be their e-mail address you want to send to and the last **must** be the group you want to put them in. The group names are arbitrary so you can use them for departments or whatever you want. If you don't need multiple groups, put everyone in the same group.

### E-mail Templates (test.html)
The e-mail to be sent **must** be in HTML format (since we want to get people to click our link) and there are variables which will be replaced by GRAPH:

  - FROM (The name of the person it will appear from)
  - NAME (The name of the person we're sending to)
  - EMAIL (The e-mail address of the person we're sending to)
  - URL (The full URI [without the hash] that we want our user to click)
  - HASH (The hash area of our link) 
  - PERSON (The friendly name of the person you're impersonating)

# Usage
```
graph.sh -i Input.CSV -f ,
```
GRAPH takes two (or three) arguments:

  - '-i' is the input CSV file
  - '-f' is the field separator used in the CSV file
  - '-x' is used to load a previously configured CSV file and proceed to launch, skipping configuration

# TODO and Bugs

TODO

  - More strict checking on inputs
  - More feedback to the user and/or logs

Bugs

  - Yes
  
### Contrib and Contact

 - You can reach me on Twitter at https://twitter.com/Grave_Rose or e-mail me at tcpdump101 -at- gmail [dot] com
 - Feel free to create suggestions however pull requests will be denied for now. I plan to open this up later on so please don't spam me on when this will happen.  
 - Monetary donations can be made here: https://www.paypal.me/tcpdump101 or with BTC at https://www.blockchain.com/btc/address/1AYBCHhoHc4SfgVuwAXZuhF1B4FWXajynG or LTC at https://live.blockcypher.com/ltc/address/LMGPbbUeHxewcBkNTCxSoRqmrF5LNcGfYT/
